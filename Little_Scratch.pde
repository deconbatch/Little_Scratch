/**
 * Little Scratch.
 * draws a node garden with specific slant lines.
 *
 * Processing 3.5.3
 * @author @deconbatch
 * @version 0.1
 * created 0.1 2020.04.19
 */

void setup() {

  size(980, 980, P2D);
  colorMode(HSB, 360, 100, 100, 100);
  smooth();
  noLoop();

}

void draw() {
  
  int   frmMax  = 3;  // draw three images
  int   copyMax = floor(random(2, 7));
  float slant   = random(HALF_PI);
  float baseHue = random(360.0);

  for (int frmCnt = 0; frmCnt < frmMax; frmCnt++) {

    copyMax++;
    copyMax %= 5;
    copyMax += 2;
    slant += HALF_PI;
    slant %= PI;
    baseHue += 120.0;

    ArrayList<PVector> pvs = calcShape(floor(random(40)), floor(random(20.0, 40.0)));

    // draw scratch
    PGraphics pg = createGraphics(width * 2, height * 2, P2D);
    pg.beginDraw();
    pg.colorMode(HSB, 360, 100, 100, 100);
    pg.background(0.0, 0.0, 0.0, 0.0);
    pg.translate(width, height);
    pg.blendMode(BLEND);
    pg = drawScratchs(pg, pvs, slant, baseHue, 1.0);
    pg.blendMode(SCREEN);
    pg = drawScratchs(pg, pvs, slant, baseHue, 0.1);
    pg.endDraw();

    // copy scratch image while rotating
    blendMode(BLEND);
    background((baseHue + 180.0) % 360.0, 5.0, 90.0, 100.0);
    pushMatrix();
    translate(width * 0.5, height * 0.5);
    rotate(random(PI));
    for (int i = 0; i < copyMax; i++) {
      rotate(TWO_PI / copyMax);
      image(pg, -width * 1.0, -height * 1.0);
    }
    popMatrix();

    casing();
    saveFrame("frames/" + String.format("%04d", frmCnt + 1) + ".png");
  }
  exit();
}

/**
 * calcShape : calculate some shape and stock these in PVector ArrayList.
 * @param  _seed    : selection seed of parameters of De Jong attractor.
 * @param  _calcMax : calculation number.
 * @return calculated results as ArrayList<PVector>.
 */

ArrayList calcShape(int _seed, int _calcMax) {

  Dna   dna      = new Dna();
  int   dnaMax   = floor(random(25, 35));
  float pDivFrom = random(0.01, 0.2);
  float pDivTo   = 0.25;
  ArrayList<PVector> pvs = new ArrayList<PVector>();

  for (int dnaCnt = 0; dnaCnt < dnaMax; dnaCnt++) {
    float[] dnas = dna.getDna(dnaCnt + _seed);
    float djA    = dnas[0];
    float djB    = dnas[1];
    float djC    = dnas[2];
    float djD    = dnas[3];
    float pDiv   = map(dnaCnt, 0, dnaMax, pDivFrom, pDivTo);

    float prevX = 0.0;
    float prevY = 0.0;
    float currX = 0.0;
    float currY = 0.0;
    for (int calcCnt = 0; calcCnt < _calcMax; calcCnt++) {

      currX += pDiv * (sin(djA * prevY) - cos(djB * prevX));
      currY += pDiv * (sin(djC * prevX) - cos(djD * prevY));
      pvs.add(new PVector(currX, currY));

      prevX = currX;
      prevY = currY;
    
    }
  }
  return pvs;
}

/**
 * drawScratchs : draws node garden with specific slant.
 * @param  _pg      : drawing target.
 * @param  _pvs     : draw with these points.
 * @param  _slant   : condition to draw line.
 * @param  _baseHue : draw color.
 * @param  _bold    : line weight.
 */
PGraphics drawScratchs(PGraphics _pg, ArrayList<PVector> _pvs, float _slant, float _baseHue, float _bold) {

  PGraphics pg = _pg;
  float baseWeight = 0.2;
  float baseRadius = min(width, height) / 5.5;
  float divSlant   = PI * 0.02;

  pg.noFill();
  for (PVector fpv : _pvs) {
    for (PVector tpv : _pvs) {

      float distance = dist(fpv.x, fpv.y, tpv.x, tpv.y);
      float valSlant = _slant * distance;
      float slant = atan2(fpv.x - tpv.x, fpv.y - tpv.y);

      if (
          distance > 0.3 && distance < 0.4 &&
          (((slant) > valSlant - divSlant && (slant) < valSlant + divSlant) ||
           ((slant) > valSlant + HALF_PI - divSlant && (slant) < valSlant + HALF_PI + divSlant))
          ) {
        float lHue = _baseHue + noise(fpv.x * 0.5, fpv.y * 0.5) * 360.0;
        float lWgt = _bold * baseWeight * pow(dist(fpv.x, fpv.y, 0.0, 0.0) + dist(tpv.x, tpv.y, 0.0, 0.0), 2);
        pg.strokeWeight(lWgt);
        pg.stroke(
                  lHue % 360.0,
                  noise(10.0, tpv.x, tpv.y) * 60.0 + 10.0,
                  30.0 + noise(fpv.x, fpv.y) * 30.0,
                  100.0
                  );
        pg.line(
                baseRadius * fpv.x,
                baseRadius * fpv.y,
                baseRadius * tpv.x,
                baseRadius * tpv.y
                );
      }
    }
  }

  return pg;
}

/**
 * casing : draw fancy casing
 */
private void casing() {
  
  fill(0.0, 0.0, 0.0, 0.0);
  strokeWeight(50.0);
  stroke(0.0, 0.0, 30.0, 100.0);
  rect(0.0, 0.0, width, height);
  strokeWeight(44.0);
  stroke(0.0, 0.0, 90.0, 100.0);
  rect(0.0, 0.0, width, height);
    
}

/**
 * Dna : hold nice shape parameters.
 */
class Dna {

  ArrayList<float[]> dnaList;

  Dna() {

    float[][] dnas = {
      {1.573155, -0.97604156, -0.49153805, -1.7673008},
      {-1.5568858, -1.5821334, -1.2908077, -1.0470141},
      { 1.5649674 , 1.2574794 , -1.3866975 , 1.173096 },
      { 0.9671733 , 1.4473511 , -1.5457076 , 0.95127994 },
      { 1.0348322 , 0.81519204 , -1.1046926 , 1.5648632 },
      { 1.4077392 , -0.9176125 , -1.53935 , -1.5119051 },
      { 1.3408806 , -0.9036783 , 1.3128784 , 0.79536873 },
      {-0.6002734, 1.6509838, 1.8099892, -1.0964627},
      {-1.0119417, 0.9379139, 1.3950387, 1.7682558},
      {-1.2209034, -1.9364693, 1.624037, -0.89463496},
      {-5.6786957, -0.93300486, -4.7100835, -0.86868095},
      {3.0192666, 5.01946, 0.23271036, 4.283874},
      { 1.5101616 , 1.3572946 , -0.89910567 , 1.4545844 },
      { 1.0751489 , 1.3426301 , -1.2923251 , -1.3146731 },
      { 1.196357 , -1.3442354 , -1.0326693 , -1.5694741 },
      { 1.526092 , 1.3366599 , -1.5353475 , 1.3622558 },
      { -1.9639189, -1.0532432, -1.9515417, -1.8730514 },
      {0.17204833, -1.7023778, 0.56470084, 1.963506},
      {-1.7607841, 0.13966942, -5.155236, 1.4487796},
      {-6.049018, 0.49595308, -1.7822785, -1.1959248},
      {-4.2884464, 0.42032194, -5.962751, 0.9008169},
      {-2.9124858, -1.4772496, -0.30669212, -4.9316893},
      {-5.3870864, 0.39404726, 1.0241723, 0.13013983},
      {-3.1428738, -4.468939, -6.256246, 1.3021169},
      {-2.870747, -2.4616163, -4.7040024, -2.4886005},
      {0.9220147, -2.0222144, -3.1428738, -4.468939},
      {-6.256246, 1.3021169, -2.870747, -2.4616163},
      {-4.7040024, -2.4886005, 0.9220147, -2.0222144},
      {0.1540699, -4.540351, -4.6457214, -3.8987062},
      {-1.7874193, -0.72684574, -3.1294382, -1.4022603},
      {-5.1447663, -4.6244364, -4.069439, -5.714358},
      {-2.928878, 0.4832754, -5.476948, 0.911829},
      {-3.372396, -2.7084184, -0.048303604, -5.2428102},
      {-5.2290826, -2.9551427, -0.7620654, 0.34173155},
      {-0.8842368, -5.7219005, -2.2207985, -1.7856398},
      {-3.2614524, -2.27459, -2.7425416, -1.407464},
      {-3.6108994, -5.235996, -1.368794, -5.20981},
      {-0.7145872, -5.5336375, -4.5006967, -0.41348505},
      {-4.160388, 0.6020603, -5.830311, -3.9082444},
      {-3.001356, -3.566355, -0.6485257, -5.3272343},
      {-3.8158128, -2.0423813, -5.5336375, -4.5006967}
    };

    dnaList = new ArrayList<float[]>();
    for (float[] dna : dnas) {
      dnaList.add(dna);
    };

  }

  float[] getDna(int i) {
    float[] dnaChose = dnaList.get(i % dnaList.size());
    return dnaChose;
  }

  float[] getRandomDna() {
    int choice = floor(random(dnaList.size()));
    float[] dnaChose = dnaList.get(choice);
    dnaList.remove(choice);
    return dnaChose;
  }

}
